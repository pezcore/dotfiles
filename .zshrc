
export LESS="-RFX"
# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# TODO: Not sure if this is needed so commenting it out. Figure out if it's
# needed or not:
# autoload -U compinit && compinit

source ~/.zplug/init.zsh
zplug "plugins/git", from:oh-my-zsh
zplug "plugins/vi-mode", from:oh-my-zsh
zplug "plugins/docker", from:oh-my-zsh
zplug "plugins/colored-man-pages", from:oh-my-zsh

zplug "zsh-users/zsh-syntax-highlighting", defer:2
zplug "zsh-users/zsh-completions", defer:2
zplug load

# User configuration

# Some path manipulation
path=($HOME/.local/bin $path)
cdpath=(. ~ ~/src)


# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"


# some more ls aliases
alias l='lsd -l --group-dirs first'
alias lx='l -X'
alias lt='l -t'
alias ltr='l --tree'
alias la="l -a"
alias ltree='exa -lbhgFT --git --color=always --time-style=long-iso'
alias trc=transmission-remote-cli
alias jnew='dt=$(date +%Y%m%d_%H%M); echo $dt > $dt && gvim $dt'
alias pylab='ipython --pylab'
alias cfn="printf '\e]710;%s\007'"
alias gch="git rev-parse --short HEAD"
alias hd="xxd -a -u"

alias -g :L='| less'
alias -g :H='| head'
alias -g :T='| tail'
alias -g :G='| grep'
alias lsext='find . -type f | perl -ne '"'"'print $1 if m/\.([^.\/]+)$/'"'"' | sort -u'

note () {
    dt=$(date +%Y%m%d_%H%M)
    echo ${dt}_$1
    nvim ${dt}_$1.md
}

alias glo='git log --pretty=glofmt'
alias gls="git ls-files -s"
alias tprint='enscript -b "%D{%Y%m%d %H:%M:%S}|\$n|\$%/\$="'
alias today='date +%Y%m%d'
alias lg=lazygit

# xbps aliases
alias xi="sudo xbps-install"
alias xrm="sudo xbps-remove"
alias xq=xbps-query

# cargo aliase
alias cc="cargo c"
alias cb="cargo b"
alias ct="cargo t"
alias ctl="cargo t --lib"
alias cr="cargo r"

# nvim alias
alias nv="nvim"
alias poe="poetry"
alias py=python
alias ipy=ipython


bindkey -a k up-line-or-beginning-search
bindkey -a j down-line-or-beginning-search
bindkey -M viins 'jk' vi-cmd-mode

alias todo=todo.sh


eval "$(starship init zsh)"
eval "$(zoxide init zsh)"

# Unset LS_COLORS because it causes directories to be printed in an ugly blue
# in tmux. Not really sure why this happens but it didn't used to. For now this
# solution does seem to work
unset LS_COLORS

## FZF setup and conifg ----------------------------------------------------------------
# reference: https://www.youtube.com/watch?v=mmqDYw9C30I

# Set up fzf key bindings and fuzzy completion
eval "$(fzf --zsh)"

export FZF_DEFAULT_COMMAND="fd --hidden --strip-cwd-prefix --exclude .git"
export FZF_DEFAULT_OPTS="--preview 'bat -n --color=always --line-range :500 {}'"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_OPTS="--preview 'bat -n --color=always --line-range :500 {}'"
export FZF_ALT_C_COMMAND="fd --type=d --hidden --strip-cwd-prefix --exclude .git"

_fzf_compgen_path() {
  fd --hidden --exclude .git . "$1"
}

_fzf_compgen_dir() {
  fd --type=d --hidden --exclude .git . "$1"
}

## thefuck -----------------------------------------------------------------------------
if command -v thefuck &> /dev/null
then
  eval $(thefuck --alias)
  eval $(thefuck --alias fk)
fi

## Television
whence tv && source <(tv init zsh)

## Yazi
function y() {
	local tmp="$(mktemp -t "yazi-cwd.XXXXXX")" cwd
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(command cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		builtin cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

fastfetch
