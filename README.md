# dotfiles

This is a repo containing my dotfiles as well as some personal notes I've taken about Linux system configuration. The dotfiles themselves primarily reside in `.config`, except for a very small number of them which reside at the repo root due to having the intended installation target in the user's home directory.

# Installation

The easiest way to install the dotfiles is with [GNU stow](https://www.gnu.org/software/stow/) which is a 3rd party "symlink farm manager". To automatically install all the dotfiles use:

```bash
stow --no-folding -t ${XDG_CONFIG_HOME:-$HOME/.config} .config
```

This will create all the necessary symbolic links under `~/.config` which point to the source files in this repo. This symlink-based installation method is recommended because it is automated (via `stow`) and because configuration changes can be automatically tracked via this repo. Note that symbolic link-based installation methods require the source repo (i.e. this repo) to be static: if you move this repo after creating the symlinks, they will all break and you will have to relink them.


## Manual Installation
If you can't or wont use `stow`, you can install the configs manually by either creating symbolic links or copying files to your config directory, for example:

```bash
ln -s /path/to/thie/repo/.config/alacritty/alacritty.toml ~/.config/alacritty/alacritty.toml
```

or

```bash
cp /path/to/thie/repo/.config/alacritty/alacritty.toml ~/.config/alacritty/alacritty.toml
```

# New System Initialization

This repo contains a list of recommended software to install on a brand-new [Void Linux](https://voidlinux.org) installation. To install it, run:

```bash
sudo xbps-install $(cat new-system-init/install-list-void.txt)
```
