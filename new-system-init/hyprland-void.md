## Installation of Hyprland on void musl

source: https://www.youtube.com/watch?v=-tCdeY1Jqk0&t=1274s
```bash
# Install deps
xbps-install git vim kitty alacritty xorg mesa-dri dbus seatd polkit elogind

cd ~/.local/pkgs

# clone these two repos
git clone --depth 10 https://github.com/void-linux/void-packages.git
# at the time of writing, the master branch of this doesn't work, so use tag v0.35.0_1 instead for now
# In the future the master branch may work
git clone -b v0.35.0_1 https://github.com/Makrennel/hyprland-void.git

void-packages/xbps-src binary-bootstrap

cat hyprland-void/common/shlibs >> void-packages/common/shlibs
cp -r hyprland-void/srcpkgs/* void-packages/srcpkgs

void-packages/xbps-src pkg hyprland
void-packages/xbps-src pkg xdg-desktop-portal-hyprland
void-packages/xbps-src pkg hyprland-protocols
```

Once all that stuff is built, install Hyprland:

```bash
xbps-install -R hostdir/binpkgs hyprland
xbps-install -R hostdir/binpkgs hyprland-protocols
xbps-install -R hostdir/binpkgs xdg-desktop-portal-hyprland
```

Start the services

```bash
sudo ln -s /etc/sv/dbus /var/service
sudo ln -s /etc/sv/polkitd /var/service
sudo ln -s /etc/sv/seatd /var/service
sudo usermod -aG _seatd tom # or whatever your user is
