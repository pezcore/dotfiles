#! /bin/bash

# This script is for installing PIA on void linux. Out of the box, PIA is
# configured to install with systemd services. Since void linux does not have
# systemd, a couple of modification to the installation process are necessary
# to to configure PIA to work with runit instead of systemd. This script
# performs that setup.

set -euo pipefail

curl -LO https://installers.privateinternetaccess.com/download/pia-linux-3.5.7-08120.run
chmod +x pia-linux-3.5.7-08120.run
./pia-linux-3.5.7-08120.run -- --skip-service

mkdir -p /etc/sv/piavpn/

cat <<EOF >/etc/sv/piavpn/run
#!/bin/sh

exec /opt/piavpn/bin/pia-daemon
EOF

chmod +x /etc/sv/piavpn/run
ln -s /etc/sv/piavpn /var/service

# After installtion, start the VPN daemon with `sv start piavpn` and run the client with
# /opt/piavpn/bin/pia-client
