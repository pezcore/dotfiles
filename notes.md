## mDNS / musl

Resolving a hostname's IP address for hosts on the local network using `hostname.local`,
as in a `ssh` or `ping` command requires the use of `nss-mdns`, which is only available
on `glibc` systems because it is a component of glibc. `musl` does not support this. The
only way workaround I'm aware of for this is configuring the server for static IP
addressing in the router's DHCP config, and then adding this IP address mapping to
musl client's `/etc/hosts` file. Maybe in the future `musl` will add support for it's
own mDNS solution.
