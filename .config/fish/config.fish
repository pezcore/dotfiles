if status is-interactive
    # Commands to run in interactive sessions can go here
    fish_vi_key_bindings
    zoxide init fish | source
    starship init fish | source
    #fzf --fish | source
    tv init fish | source
    bind -M insert \cT tv_smart_autocomplete
    bind -M insert \cR tv_shell_history
end

set CDPATH . ~ ~/src
set -gx LESS -RFX
set -gx EDITOR nvim

set -gx FZF_DEFAULT_COMMAND "fd --hidden --strip-cwd-prefix --exclude .git"
set -gx FZF_DEFAULT_OPTS "--preview 'bat -n --color=always --line-range :500 {}'"
set -gx FZF_CTRL_T_OPTS "--preview 'bat -n --color=always --line-range :500 {}'"

# ls aliases
alias l='lsd -l --group-dirs first'
alias lx='l -X'
alias lt='l -t'
alias ltr='l --tree'
alias la="l -a"
alias ltree='exa -lbhgFT --git --color=always --time-style=long-iso'
abbr -a lsext 'find . -type f | grep -i -E -o \'\.\w*$\' | sort | uniq -c'

# cargo aliase
alias cc="cargo c"
alias cb="cargo b"
alias ct="cargo t"
alias ctl="cargo t --lib"
alias cr="cargo r"

# common commands aliases
alias nv="nvim"
alias poe="poetry"
alias py=python
alias ipy=ipython
alias today="date +%Y%m%d"

# git alias
abbr -e glo # Need to erase the glo abbr set by git-plugin
alias glo='git log --pretty=glofmt'
alias gls="git ls-files -s"
alias gch="git rev-parse --short HEAD"
alias gcd="cd (git rev-parse --show-toplevel)"
alias gdup="git diff @{u}"

abbr -a :L --position anywhere '| less'
abbr -a :H --position anywhere '| head'
abbr -a :T --position anywhere '| tail'
abbr -a :G --position anywhere '| grep'

abbr -a ... --position anywhere ../..
abbr -a .... --position anywhere ../../..
abbr -a ..... --position anywhere ../../../..

abbr -a ff fastfetch

function sus
    if set -q SSH_CLIENT; or set -q SSH_TTY
        echo "Remote connection detected, refusing to suspend"
    else
        loginctl suspend
    end
end

function y
    set tmp (mktemp -t "yazi-cwd.XXXXXX")
    yazi $argv --cwd-file="$tmp"
    if set cwd (command cat -- "$tmp"); and [ -n "$cwd" ]; and [ "$cwd" != "$PWD" ]
        builtin cd -- "$cwd"
    end
    rm -f -- "$tmp"
end

function _which
    command --search (string sub --start=2 $argv)
end
abbr -a = --position anywhere --regex='=\w+' --function _which
