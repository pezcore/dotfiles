# Volumes
abbr -a dvc "docker volume create"
abbr -a dvl "docker volume ls"
abbr -a dvrm "docker volume rm"
abbr -a dvp "docker volume prune -f"
abbr -a dvi "docker volume inspect"

# Networks
abbr -a dnl "docker network ls"
abbr -a dni "docker network inspect"
abbr -a dnrm "docker network rm"
abbr -a dnp "docker network prune -f"

# Docker Compose
abbr -a dcv "docker-compose -v"
abbr -a dcu "docker-compose up"
abbr -a dcd "docker-compose down"
abbr -a dcb "docker-compose build --no-cache"
abbr -a dcc "docker-compose config"

# Images
abbr -a dil "docker images"
abbr -a dls docker images
abbr -a dip "docker image prune"
abbr -a dipf "docker image prune -f"
abbr -a drmi "docker rmi"

# Containers
abbr -a dps docker ps
abbr -a dpsa docker ps -a
abbr -a dst docker start
abbr -a drm docker rm

# misc
abbr -a dx docker exec -it
